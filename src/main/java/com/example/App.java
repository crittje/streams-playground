package com.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Started app!" );
        Streams streams = new Streams();
        streams.startStreams();
        System.out.println("End app");
    }
}
