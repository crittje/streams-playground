package com.example;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Streams {

    Streams() {

    }

    void startStreams() {
        int[] intArray = new int[] {1, 2, 3};
        String[] stringArray = new String[] {"c2", "b2", "a2"};
        String[] namesArray = new String[] {"Jan", "Joost", "Chris", "Niels"};

        System.out.println("The modified average = " + modifiedAverage(intArray));
        System.out.println("The found string = " + stringFiltering(stringArray));
        System.out.println("List of names = " + collectParticularNames(namesArray, "J").toString());
    }

    public double modifiedAverage(int[] intArray) {

        return Arrays.stream(intArray)
                .map(n  -> 2 * n + 1)
                .average()
                .getAsDouble();
    }

    public String stringFiltering(String[] stringArray) {

        return Stream.of(stringArray)
                .filter(s -> s.startsWith("a"))
                .findFirst()
                .get();
    }

    public List<String> collectParticularNames(String[] namesArray, String firstChar) {

        return Stream.of(namesArray)
                .filter(name -> name.startsWith(firstChar))
                .collect(
                        Collectors.toList()
                );
    }
}
