package com.example;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class StreamsTest {

    private int[] intArrays;
    private String[] stringArray;
    private String[] namesArray;
    private Streams streams;

    @Before
    public void setUp() {
        intArrays = new int[] {1, 2, 3};
        stringArray = new String[] {"c2", "b2", "a2"};
        namesArray = new String[] {"Jan", "Joost", "Chris", "Niels"};
        streams = new Streams();
    }

    @Test
    public void testModifiedAverage() {

        final int expected = 5;
        final int actual = (int) streams.modifiedAverage(intArrays);

        assertEquals(expected, actual);

    }

    @Test
    public void testStringFilter() {

        final String expected = "a2";
        final String actual = streams.stringFiltering(stringArray);

        assertEquals(expected, actual);
    }

    @Test
    public void testCollectParticularNames() {

        final String firstChar = "J";
        final List<String> actual = streams.collectParticularNames(namesArray, firstChar);

        assertEquals(2, actual.size());
    }
}
